package animal;
import animal.Animal;

public class Cats extends Animal {
	private String[] voice = {"Miaaaw..", "Purrr..", "Mwaw!", "Mraaawr!"};

	public Cats(String name, int length) { 
		super(name, length);
		if (this.getLength() < 45) {
            this.cageType = "A";
        } else if (this.getLength() <= 60) {
            this.cageType = "B";
        } else {
            this.cageType = "C";
        }
	}

	public void brushFur() {
		System.out.println("Time to clean " + this.name + "'s fur\n" +
						this.name + " makes a voice: Nyaaan...\n" +
						"Back to the office!\n");
	}

	public void cuddle() {
		int randomVoice = (int)(Math.random() * 4);
		System.out.println(this.name + " makes a voice: " + voice[randomVoice] + 
						"\nBack to the office!\n");
	}
}