package animal;
import animal.Animal;

public class Lion extends Animal {

	public Lion(String name, int length) {
		super(name, length);
		if (this.getLength() < 75) {
            this.cageType = "A";
        } else if (this.getLength() <= 90) {
            this.cageType = "B";
        } else {
            this.cageType = "C";
        }
	}

	public void hunt() {
		System.out.println("Lion is hunting..\n" + 
						this.name + " makes a voice: err...!\n" + 
						"Back to the office!\n");
	}

	public void brushMane() {
		System.out.println("Clean the lion's mane..\n" + 
						this.name + " makes a voice: Hauhhmm!\n" + 
						"Back to the office!\n");
	}

	public void disturb() {
		System.out.println(this.name + " makes a voice: HAUHHMM!\n" +
						"Back to the office!\n");	
	}
}