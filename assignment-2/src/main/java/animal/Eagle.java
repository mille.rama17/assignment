package animal;
import animal.Animal;

public class Eagle extends Animal {

	public Eagle(String name, int length) {
		super(name, length);
		if (this.getLength() < 75) {
            this.cageType = "A";
        } else if (this.getLength() <= 90) {
            this.cageType = "B";
        } else {
            this.cageType = "C";
        }
	}

	public void fly() {
		System.out.println(this.name + " makes a voice: kwaakk...\n" + 
						"You hurt!\n" + 
						"Back to the office!\n");
	}
}