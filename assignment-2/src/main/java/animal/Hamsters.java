package animal;
import animal.Animal;

public class Hamsters extends Animal {

	public Hamsters(String name, int length) {
		super(name, length);
		if (this.getLength() < 45) {
            this.cageType = "A";
        } else if (this.getLength() <= 60) {
            this.cageType = "B";
        } else {
            this.cageType = "C";
        }
	}

	public void gnaw() {
		System.out.println(this.name + " makes a voice: ngkkrit.. ngkkrrriiit\n" + 
						"Back to the office!\n");
	}

	public void wheel() {
		System.out.println(this.name + " makes a voice: trrr.... trrr...\n" + 
						"Back to the office!\n");
	}

}