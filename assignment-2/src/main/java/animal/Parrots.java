package animal;
import animal.Animal;

public class Parrots extends Animal {

	public Parrots(String name, int length) {
		super(name, length);
		if (this.getLength() < 45) {
            this.cageType = "A";
        } else if (this.getLength() <= 60) {
            this.cageType = "B";
        } else {
            this.cageType = "C";
        }
	}

	public void fly() {
		System.out.println("Parrot " + this.name + " flies!\n" + 
						this.name + " makes a voice: FLYYYY....\n" + 
						"Back to the office!\n");
	}
	public void conversation(String talk) {
		System.out.println(this.name + " says: " + talk.toUpperCase() +
						"\nBack to the office!\n");			
	}

	public void doNothing() {
		System.out.println(this.name + " says: HM?\n" + 
						"Back to the office!\n");
	}
}