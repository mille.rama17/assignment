package animal;

public class Animal {
	protected String name;
	protected int length;
	protected String cageType;

	public Animal(String name, int length) {
		this.name = name;
		this.length = length;
	}

	public String getName() {
		return this.name;
	}

	public int getLength() {
		return this.length;
	}

	public String getType() {
		return this.cageType;
	}

	public void doNothing() {
		System.out.println("You do nothing...\n" + 
						"Back to the office!\n");
	}

	public String toString() {
		return this.getName() + " (" + this.getLength() + " - " + this.getType() + "), ";
	}
}