package cages;
import animal.*;
import java.util.ArrayList;


public class Cages {
	private Animal[] cage;
	private Animal cageList[][] = new Animal[3][];
	private Animal sortedList[][] = new Animal[3][];
	private String location;
	
	public Cages(ArrayList<Animal> cages, int i) {
		this.cage = cages.toArray(new Animal[cages.size()]);
		if (i == 0) {
            this.location = "outdoor";
        } else {
            this.location = "indoor";
        }
	}

	public String getLocation() {
		return this.location;
	}

	public Animal[] getList() {
		return cage;
	}

	public Animal[][] getCageList() {
		return cageList;
	}

	public Animal[][] getSortedList() {
		return sortedList;
	}

	public void arrangeCage() {
		int size = cage.length;
		int count = 0;
		if (size > 3) {
            for (int i = 0; i < 3; i++) {
                Animal[] temp = new Animal[(i < size / 3 - size % 3) ? size / 3 : size / 3 + 1];
                for (int j = 0; j < size / 3 + 1; j++) {
                    if (i < size / 3 - size % 3) {
                        if (j == size / 3) {
                            break;
                        }
                    }
                    temp[j] = cage[count++];
                }
                cageList[i] = temp;
            }
        } else {
            for (int i = 0; i < size; i++) {
                Animal[] temp = new Animal[1];
                temp[0] = cage[i];
                cageList[i] = temp;
            }
        }
	}

	public void reArrangeCage() {
		int outSize = cageList.length;
        int inSize = 0;
        for (int i = 0; i < outSize; i++) {
            Animal[] temp;
            if (cageList[(i + 2) % 3] != null) {
                inSize = this.cageList[(i + 2) % 3].length;
                temp = new Animal[cageList[(i + 2) % 3].length];
                for (int j = 0; j < inSize; j++) {
                    temp[j] = cageList[(i + 2) % 3][inSize - 1 - j];
                }
            } else {
                temp = new Animal[1];
            }
            sortedList[i] = temp;
		}
	}
}