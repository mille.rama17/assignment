import animal.*;
import cages.Cages;
import java.util.Scanner;
import java.util.ArrayList;

public class JavariPark {
	private static ArrayList<Animal> catList = new ArrayList<>();
	private static ArrayList<Animal> lionList = new ArrayList<>();
	private static ArrayList<Animal> parrotList = new ArrayList<>();
	private static ArrayList<Animal> eagleList = new ArrayList<>();
	private static ArrayList<Animal> hamsterList = new ArrayList<>();
	private static Cages cage[] = new Cages[5];

	public static void main(String[] args) {
		Scanner input = new Scanner(System.in);
		String animalType[] = {"cat", "lion", "eagle", "parrot", "hamster"};
		ArrayList<ArrayList<Animal>> animalList = new ArrayList<ArrayList<Animal>>();
		animalList.add(hamsterList);
		animalList.add(parrotList);
		animalList.add(eagleList);
		animalList.add(lionList);
		animalList.add(catList);
		System.out.println("Welcome to Javari Park!");
		System.out.println("input the number of animals");
		Animal temp;
		for (int i = 0; i < 5; i++) {
			System.out.print(animalType[i] + ": ");
			int numInput = Integer.parseInt(input.next());
			if (numInput > 0) {
				System.out.println("Provide the information of " + animalType[i] + "(s)");
				String arrayInput[] = input.next().split(",");
				for (int j = 0; j < arrayInput.length; j++) {
					String arrayInput2[] = arrayInput[j].split("\\|");
					String nameInput = arrayInput2[0];
					int lengthInput = Integer.parseInt(arrayInput2[1]);
					if (animalType[i].equals("cat")) {
						temp = new Cats(nameInput, lengthInput);
						catList.add(temp);	
					} else if (animalType[i].equals("lion")) {
						temp = new Lion(nameInput, lengthInput);
						lionList.add(temp);
					} else if (animalType[i].equals("eagle")) {
						temp = new Eagle(nameInput, lengthInput);
						eagleList.add(temp);
					} else if (animalType[i].equals("parrot")) {
						temp = new Parrots(nameInput, lengthInput);
						parrotList.add(temp);
					} else if (animalType[i].equals("hamster")) {
						temp = new Hamsters(nameInput, lengthInput);
						hamsterList.add(temp);
					}
				}
			}
			for (int j = 0; j < 5; j++) {
					if (animalType[i].equals("cat")) {
						cage[4] = new Cages(catList, 1);
					} else if (animalType[i].equals("lion")) {
						cage[3] = new Cages(lionList, 0);
					} else if (animalType[i].equals("eagle")) {
						cage[2] = new Cages(eagleList, 0);
					} else if (animalType[i].equals("parrot")) {
						cage[1] = new Cages(parrotList, 1);
					} else if (animalType[i].equals("hamster")) {
						cage[0] = new Cages(hamsterList, 1);
					}
			}
		}
		System.out.println("Animals have been successfully recorded!");
		System.out.println("\n=============================================");
		System.out.println("Cage arrangement:");
		for (int i = cage.length - 1; i >= 0; i--) {
			if (cage[i].getList().length == 0) {
				continue;
			} else {
	            if (cage[i] != null) {
	                System.out.println("location: " + cage[i].getLocation());
	                cage[i].arrangeCage();
	                for (int j = cage[i].getCageList().length - 1; j >= 0; j--) {
	                    System.out.print("Level " + (j + 1) + ": ");
	                    if (cage[i].getCageList()[j] != null) {
	                        for (Animal k : cage[i].getCageList()[j]) {
	                            System.out.print((k != null) ? k : "");
	                        }
	                    }
	                    System.out.println();
	                }
	                cage[i].reArrangeCage();
	                System.out.println("\nAfter rearrangement...");
	                for (int j = cage[i].getSortedList().length - 1; j >= 0; j--) {
	                    System.out.print("Level " + (j + 1) + ": ");
	                    for (Animal k : cage[i].getSortedList()[j]) {
	                        System.out.print((k != null) ? k : "");
	                    }
	                    System.out.println();
	                }
	                System.out.println();
	            }				
			}

        }
		System.out.println("NUMBER OF ANIMALS:");
		String animalType2[] = {"cat", "lion", "parrot", "eagle", "hamster"};
		ArrayList<ArrayList<Animal>> animalList2 = new ArrayList<ArrayList<Animal>>();
		animalList2.add(catList);
		animalList2.add(lionList);
		animalList2.add(parrotList);
		animalList2.add(eagleList);
		animalList2.add(hamsterList);
		for (int i = 0; i < animalList.size(); i++) {
			System.out.println(animalType2[i] + ":" + animalList2.get(i).size());
		}
		System.out.println("\n=============================================");
		String visitType[] = {"Cat", "Eagle", "Hamster", "Parrot", "Lion", "Exit"};
		boolean notExit = false;
		do {
			System.out.println("Which animal you want to visit?");
			System.out.println("(1: Cat, 2: Eagle, 3: Hamster, 4: Parrot, 5: Lion, 99:exit)");
			int numVisit = Integer.parseInt(input.next());
			if (numVisit == 1) {
				System.out.print("Mention the name of cat you want to visit: ");
				String nameVisit = input.next();
				if (checkAnimal(nameVisit, catList)) {
					int x = findIndex(nameVisit, catList);
				 	System.out.println("You are visiting " + nameVisit + " (cat) now, what would you like to do?");
				 	System.out.println("1: Brush the fur 2: Cuddle");
				 	int action = Integer.parseInt(input.next());
				 	if (action == 1) {
				 		Cats y = (Cats)catList.get(x);
				 		y.brushFur();
				 	} else if (action == 2) {
				 		Cats y = (Cats)catList.get(x);
				 		y.cuddle();
				 	} else {
				 		Cats y = (Cats)catList.get(x);
				 		y.doNothing();
				 	}
				} else {
					System.out.println("There is no cat with that name! Back to the office!\n");
				} 
			} else if (numVisit == 2) {
				System.out.print("Mention the name of eagle you want to visit: ");
				String nameVisit = input.next();
				if (checkAnimal(nameVisit, eagleList)) {
					int x = findIndex(nameVisit, eagleList);
				 	System.out.println("You are visiting " + nameVisit + " (eagle) now, what would you like to do?");
				 	System.out.println("1: Order to fly");
				 	int action = Integer.parseInt(input.next());
				 	if (action == 1) {
				 		Eagle y = (Eagle)eagleList.get(x);
				 		y.fly();
				 	} else {
				 		Eagle y = (Eagle)eagleList.get(x);
				 		y.doNothing();
				 	}
				} else {
					System.out.println("There is no eagle with that name! Back to the office!\n");
				} 
			} else if (numVisit == 3) {
				System.out.print("Mention the name of hamster you want to visit: ");
				String nameVisit = input.next();
				if (checkAnimal(nameVisit, hamsterList)) {
					int x = findIndex(nameVisit, hamsterList);
				 	System.out.println("You are visiting " + nameVisit + " (hamster) now, what would you like to do?");
				 	System.out.println("1: See it gnawing 2: Order to run in the hamster wheel");
				 	int action = Integer.parseInt(input.next());
				 	if (action == 1) {
				 		Hamsters y = (Hamsters)hamsterList.get(x);
				 		y.gnaw();
				 	} else if (action == 2) {
				 		Hamsters y = (Hamsters)hamsterList.get(x);
				 		y.wheel();
				 	} else {
				 		Hamsters y = (Hamsters)hamsterList.get(x);
				 		y.doNothing();
				 	}
				} else {
					System.out.println("There is no hamster with that name! Back to the office!\n");
				} 
			} else if (numVisit == 4) {
				System.out.print("Mention the name of parrot you want to visit: ");
				String nameVisit = input.next();
				if (checkAnimal(nameVisit, parrotList)) {
					int x = findIndex(nameVisit, parrotList);
				 	System.out.println("You are visiting " + nameVisit + " (parrot) now, what would you like to do?");
				 	System.out.println("1: Order to fly 2: Do conversation");
				 	int action = Integer.parseInt(input.next());
					input.nextLine();
				 	if (action == 1) {
				 		Parrots y = (Parrots)parrotList.get(x);
				 		y.fly();
				 	} else if (action == 2) {
				 		System.out.print("You say: ");
				 		String talk = input.nextLine();
				 		Parrots y = (Parrots)parrotList.get(x);
				 		y.conversation(talk);
				 	} else {
				 		Parrots y = (Parrots)parrotList.get(x);
				 		y.doNothing();
				 	}
				} else {
					System.out.println("There is no parrot with that name! Back to the office!\n");
				} 
			} else if (numVisit == 5) {
				System.out.print("Mention the name of lion you want to visit: ");
				String nameVisit = input.next();
				if (checkAnimal(nameVisit, lionList)) {
					int x = findIndex(nameVisit, lionList);
					System.out.println("You are visiting " + nameVisit + " (lion) now, what would you like to do?");
				 	System.out.println("1: See it hunting 2: Brush the mane 3: Disturb it");
				 	int action = Integer.parseInt(input.next());
				 	if (action == 1) {
				 		Lion y = (Lion)lionList.get(x);
				 		y.hunt();
				 	} else if (action == 2) {
				 		Lion y = (Lion)lionList.get(x);
				 		y.brushMane();
				 	} else if (action == 3) {
				 		Lion y = (Lion)lionList.get(x);
				 		y.disturb();
				 	} else {
				 		Lion y = (Lion)lionList.get(x);
				 		y.doNothing();
				 	}
				} else {
					System.out.println("There is no lion with that name! Back to the office!\n");
				} 
			} else if (numVisit == 99) {
				notExit = true;
			}
		} while (!notExit);
	}

	public static boolean checkAnimal(String name, ArrayList<Animal> list) {
		boolean isAnimal = false;
		for (Animal i : list) {
			if (name.equals(i.getName())) {
				isAnimal = true;
			}
		}
		return isAnimal;
	}

	public static int findIndex(String name, ArrayList<Animal> list) {
		for (int i = 0; i < list.size(); i++) {
			if (name.equals(list.get(i).getName())) {
				return i;
			}
		}
		return -1;
	}
}