package javari.reader;

import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.*;

/**
 * This class represents the base class for reading all lines from a text
 * file that contains CSV data.
 *
 * @author Programming Foundations 2 Teaching Team
 * @author TODO If you make changes in this class, please write your name here
 *     and describe the changes in the comment block
 */
public abstract class CsvReader {

    public static final String COMMA = ",";

    private final Path file;
    protected final List<String> lines;
    protected static final Map<String, List<String>> animalMap = createAnimalMap();
    protected static final Map<String, List<String>> attractionsMap = createAttractionsMap();
    protected static final Map<String, List<String>> animalMap2 = createAnimalMap2();

    /**
     * Defines the base constructor for instantiating an object of
     * {@code CsvReader}.
     *
     * @param file  path object referring to a CSV file
     * @throws IOException if given file is not present or cannot be read
     *     properly
     */
    public CsvReader(Path file) throws IOException {
        this.file = file;
        this.lines = Files.readAllLines(this.file, StandardCharsets.UTF_8);
    }

    public static Map<String, List<String>> getAnimalMap() {
        return animalMap;
    }

    public static Map<String, List<String>> createAnimalMap() {
        Map<String, List<String>> animalMap = new HashMap<>();
        animalMap.put("Explore the Mammals", new ArrayList<>(Arrays.asList("Hamster", "Lion", "Cat", "Whale")));
        animalMap.put("World of Aves", new ArrayList<>(Arrays.asList("Eagle", "Parrot")));
        animalMap.put("Reptillian Kingdom", new ArrayList<>(Arrays.asList("Snake")));
        return animalMap;
    }

    public static Map<String, List<String>> createAnimalMap2() {
        Map<String, List<String>> animalMap2 = new HashMap<>();
        animalMap2.put("mammals", new ArrayList<>(Arrays.asList("Hamster", "Lion", "Cat", "Whale")));
        animalMap2.put("aves", new ArrayList<>(Arrays.asList("Eagle", "Parrot")));
        animalMap2.put("reptiles", new ArrayList<>(Arrays.asList("Snake")));
        return animalMap2;
    }

    public static Map<String, List<String>> getAttractionsMap() {
        return attractionsMap;
    }

    public static Map<String, List<String>> createAttractionsMap() {
        Map<String, List<String>> attractionsMap = new HashMap<>();
        attractionsMap.put("Circles of Fires", new ArrayList<>(Arrays.asList("Whale", "Lion", "Eagle")));
        attractionsMap.put("Dancing Animals", new ArrayList<>(Arrays.asList("Cat", "Snake", "Parrot", "Hamster")));
        attractionsMap.put("Counting Masters", new ArrayList<>(Arrays.asList("Hamster", "Whale","Parrot")));
        attractionsMap.put("Passionate Coders", new ArrayList<>(Arrays.asList("Cat", "Hamster", "Snake")));
        return attractionsMap;
    }

    /**
     * Returns all line of text from CSV file as a list.
     *
     * @return
     */
    public List<String> getLines() {
        return lines;
    }

    /**
     * Counts the number of valid records from read CSV file.
     *
     * @return
     */
    public abstract long countValidRecords();

    /**
     * Counts the number of invalid records from read CSV file.
     *
     * @return
     */
    public abstract long countInvalidRecords();
}
