import java.awt.Dimension;
import javax.swing.ImageIcon;
import javax.swing.JButton;

public class GameCard {
    private int id;
    private ImageIcon closed;
    private ImageIcon revealed;
    private boolean isMatched = false;
    private JButton button = new JButton();

    /**
     * Constructor of the GameCard.
     * @param id of the image
     * @param closed image of the button
     * @param revealed image of the button
     */
    public GameCard(int id, ImageIcon closed, ImageIcon revealed) {
        this.id = id;
        this.closed = closed;
        this.revealed = revealed;
        this.button.setIcon(closed);
        this.button.setPreferredSize(new Dimension(100, 100));
    }

    /**
     * Method to set the button icon to closed image.
     */
    public void hide() {
        button.setIcon(closed);
    }

    /**
     * Method to set the button icon to revealed image.
     */
    public void reveal() {
        button.setIcon(revealed);
    }

    /**
     * method to set boolean isMatched.
     * @param match true if the card is matched, false if the card is not matched
     */
    public void setIsMatched(boolean match) {
        this.isMatched = match;
    }

    /**
     * method that return id of the card.
     * @return id of the card
     */
    public int getId() {
        return id;
    }

    /**
     * method that return the button of the card.
     * @return button of the card
     */
    public JButton getButton() {
        return button;
    }

    /**
     * method that return boolean isMatched.
     * @return boolean isMatched
     */
    public boolean getIsMatched() {
        return isMatched;
    }
}