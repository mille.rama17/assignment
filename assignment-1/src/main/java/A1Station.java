import java.util.Scanner;

public class A1Station {

    private static final double THRESHOLD = 250; // in kilograms

    public static void main(String[] args) {
        TrainCar last = null;
        int totalCat = 0;
        Scanner input = new Scanner(System.in);
        String jumlahKucing = input.nextLine();
        int cats = Integer.parseInt(jumlahKucing);
        for (int i = 0; i < cats; i++) {
            String[] arrayInput = input.nextLine().split(",");
            double weightInput = Double.parseDouble(arrayInput[1]);
            double lengthInput = Double.parseDouble(arrayInput[2]);
            WildCat kucing = new WildCat(arrayInput[0], weightInput, lengthInput);
            totalCat++;
            if (last == null) {
                last = new TrainCar(kucing);
            } else {
                last = new TrainCar(kucing, last);
            }
            double AverageMassIndex = last.computeTotalMassIndex() / totalCat;
            if (last.computeTotalWeight() > THRESHOLD) {
                System.out.println("The train departs to Javari Park");
                System.out.print("[LOCO]<--");
                last.printCar();
                System.out.printf("\nAverage mass index of all cats: %.2f\n", AverageMassIndex);
                if (AverageMassIndex >= 30) {
                    System.out.println("In average, the cats in the train are *obese*");
                } else if (AverageMassIndex >= 25) {
                    System.out.println("In average, the cats in the train are *overweight*");
                } else if (AverageMassIndex >= 18.5) {
                    System.out.println("In average, the cats in the train are *normal*");
                } else {
                    System.out.println("In average, the cats in the train are *underweight*");
                }
                kucing = null;
                last = null;
                totalCat = 0;
            } else if (i == cats - 1) {
                System.out.println("The train departs to Javari Park");
                System.out.print("[LOCO]<--");
                last.printCar();
                System.out.printf("\nAverage mass index of all cats: %.2f\n", AverageMassIndex);
                if (AverageMassIndex >= 30) {
                    System.out.print("In average, the cats in the train are *obese*");
                } else if (AverageMassIndex >= 25) {
                    System.out.print("In average, the cats in the train are *overweight*");
                } else if (AverageMassIndex >= 18.5) {
                    System.out.print("In average, the cats in the train are *normal*");
                } else {
                    System.out.print("In average, the cats in the train are *underweight*");
                }
            }
        }
    }
}
